﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CISP335_Gunner
{
    public class AnimatedSprite
    {
        private Texture2D m_sprite;
        private int m_cellCount;
        private int m_height;
        private int m_width;
        private int m_framerate;

        private int m_framecount;
        private int m_count;

        private int m_currentCell;
        private bool isPlaying;

        private Vector2 m_position;

        public AnimatedSprite(String SpriteFileName, int CellCount, int height, int width, int frate, Vector2 Position)
        {
            m_sprite = GameState.Content.Load<Texture2D>(SpriteFileName);
            if (CellCount > 0)
            {
                m_cellCount = CellCount;
            }
            if (height > 0)
            {
                m_height = height;
            }
            if (width > 0)
            {
                m_width = width;
            }
            if (frate > 0)
            {
                m_framerate = frate;
            }

            m_currentCell = -1;
            isPlaying = false;
            m_framecount = frate / CellCount;
            m_count = 0;
            m_position = Position;

        }

        public void Update()
        {
            if (isPlaying)
            {
                if ((m_count % m_framecount) == 0)
                {
                    if (m_currentCell != m_cellCount - 1)
                    {
                        m_currentCell++;
                    }
                    else
                    {
                        m_currentCell = 0;
                    }
                }
                m_count++;
            }

        }

        public void Draw()
        {

            Rectangle source = new Rectangle(m_currentCell * m_width, 0, m_width, m_height);
            Rectangle dest = new Rectangle((int)m_position.X, (int)m_position.Y, m_width, m_height);

            GameState.spriteBatch.Begin();

            GameState.spriteBatch.Draw(m_sprite, dest, source, Color.Red);
            


            GameState.spriteBatch.End();

        }

        public void Play()
        {
            isPlaying = true;
            //Update();
        }

        public void Pause()
        {
            isPlaying = false;
        }

        public void Stop()
        {
            isPlaying = false;
            m_currentCell = 0;
        }


    }
}
