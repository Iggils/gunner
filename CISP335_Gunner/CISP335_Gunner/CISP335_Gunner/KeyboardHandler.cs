﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace CISP335_Gunner
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class KeyboardHandler : Microsoft.Xna.Framework.GameComponent
    {

        private KeyboardState prevKBState;
        private KeyboardState currKBState;

        public KeyboardHandler(Game game)
            : base(game)
        {
            // TODO: Construct any child components here
            prevKBState = currKBState = Keyboard.GetState();
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            // TODO: Add your update code here

            prevKBState = currKBState;
            currKBState = Keyboard.GetState();

            base.Update(gameTime);
        }

        public Boolean isKeyPressed(Keys key)
        {
            //bool pressed = false;

            //if (prevKBState.IsKeyDown(key) == false && currKBState.IsKeyDown(key) == true)
            //{
            //    pressed = true;
            //}
            return (prevKBState.IsKeyDown(key) == false &&
                currKBState.IsKeyDown(key) == true);

            //return pressed;
        }

        public Boolean isKeyHeld(Keys key)
        {
            bool held = false;

            if (prevKBState.IsKeyDown(key) == true && currKBState.IsKeyDown(key) == true)
            {
                held = true;
            }

            return held;
        }

        public Boolean wasKeyReleased(Keys key)
        {
            bool released = false;

            if (prevKBState.IsKeyDown(key) == true && currKBState.IsKeyDown(key) == false)
            {
                released = true;
            }

            return released;
        }
    }
}
