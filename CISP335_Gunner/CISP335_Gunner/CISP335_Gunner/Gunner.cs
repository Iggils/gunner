using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CISP335_Gunner
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Gunner : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        //Player[] players;
        //const int MAX_PLAYERS = 2;
        AnimatedSprite animSprite, animSprite2;

        //NOTE: If MAX_PLAYERS CHANGES FROM 2, THEN ADJUST GamePlayers accordingly
        public enum GamePlayers
        {
            One = 0,
            Two
        }
       

        public Gunner()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            GameState.screenManager = new ScreenManager();
            GameState.game = this;

            //players = new Player[MAX_PLAYERS];

            //for (int i = 0; i < MAX_PLAYERS; i++)
            //{
            //    players[i] = new Player();
            //}
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            GameState.spriteBatch = new SpriteBatch(GraphicsDevice);
            GameState.Content = Content;
            GameState.screenManager.Push(new Screens.SplashScreen());
            GameState.kbHandler = new KeyboardHandler(this);
            Components.Add(GameState.kbHandler);
            GameState.auEngine = new AudioEngine(@"Content\CISP335_Gunnersounds.xgs");
            GameState.wvBank = new WaveBank(GameState.auEngine,@"Content\Wave Bank.xwb");
            GameState.sdBank = new SoundBank(GameState.auEngine, @"Content\Sound Bank.xsb");

            

            //players[(int)GamePlayers.One].Texture = GameState.Content.Load<Texture2D>(@"Sprites\ammo");
            //players[(int)GamePlayers.One].m_playerNum = Player.GamePlayers.One;

            //players[(int)GamePlayers.Two].Texture = GameState.Content.Load<Texture2D>(@"Sprites\house");
            //players[(int)GamePlayers.Two].m_playerNum = Player.GamePlayers.Two;



            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            GameState.screenManager.Top().Update(gameTime);

            //foreach (Player p in players)
            //    p.Update(gameTime);
            //if (players[(int)GamePlayers.One].Intersects(players[(int)GamePlayers.Two]))
            //{
            //    GameState.sdBank.PlayCue("explosion");

            //    animSprite2 = new AnimatedSprite(@"Sprites\animatedSprite", 3, 32, 32, 60, new Vector2(200, 300));
            //    animSprite2.Play();
            if (GameState.eog)
            {
                this.Exit();
            }

            //    //bgColor = Color.Red;
            //}
            //else
            //{
            //    // bgColor = Color.CornflowerBlue;
            //}

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            GameState.screenManager.Top().Draw(gameTime);
            
            
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
