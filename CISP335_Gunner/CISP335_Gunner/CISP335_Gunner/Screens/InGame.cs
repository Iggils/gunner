﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using System.Threading;

namespace CISP335_Gunner.Screens
{
    public class InGame : GameScreen
    {
        SpriteFont fntJingJing;
        Texture2D background;
        Texture2D cannon;
        Texture2D target;
        Texture2D ammo;
        Texture2D ammo1;
        Texture2D ammo2;
        Vector2 pos;
        Vector2 pos1;
        Vector2 pos2;
        Vector2 initial;
        Vector2 targ;
        Vector2 v = new Vector2();
        int count = 0;
        TimeSpan[] ts;
        TimeSpan[] ct;
        //creates an array of player class
        Player[] players;
        const int MAX_PLAYERS = 2;
        const int MAX_SHOTS = 3;
        AnimatedSprite animSprite, animSprite2;
        Texture2D[] amm;
        GraphicsDevice graphicDevice;
        int cntGm = 0;

        public enum GamePlayers
        {
            One = 0,
            Two
        }

        float angle;


        float velocity;
        TimeSpan startTime, currentTime, endTime;
        Boolean hasLaunched;
        Boolean hasExploded;
        Boolean gameOver;
        Boolean endofGame;


        public InGame()
        {
            fntJingJing = GameState.Content.Load<SpriteFont>(@"Font\JingJing");
            background = GameState.Content.Load<Texture2D>(@"Sprites\bg4");
            cannon = GameState.Content.Load<Texture2D>(@"Sprites\Cannon2y");
            ammo = GameState.Content.Load<Texture2D>(@"Sprites\ammo1");
            ammo1 = GameState.Content.Load<Texture2D>(@"Sprites\ammo1");
            ammo2 = GameState.Content.Load<Texture2D>(@"Sprites\ammo1");

            target = GameState.Content.Load<Texture2D>(@"Sprites\house");
            animSprite2 = new AnimatedSprite(@"Sprites\animatedSprite", 3, 32, 32, 60, new Vector2(610, 405));
            // animSprite2.Play();
            players = new Player[MAX_PLAYERS];

          
            //creates the objects to test collision with
            for (int i = 0; i < MAX_PLAYERS; i++)
            {
                players[i] = new Player();
            }

            ts = new TimeSpan[3];
            ct = new TimeSpan[3];

            velocity = 0;
            angle = 35;
            initial = new Vector2(75, 400);
            pos = new Vector2(75, 400);
            pos1 = new Vector2(75, 400);
            pos2 = new Vector2(75, 400);
            targ = new Vector2(600, 400);

            hasLaunched = false;
            hasExploded = true;
            gameOver = false;
            endofGame = false;

            players[(int)GamePlayers.One].Texture = ammo;
            players[(int)GamePlayers.One].m_playerNum = Player.GamePlayers.One;


            players[(int)GamePlayers.Two].Texture = target;
            players[(int)GamePlayers.Two].m_playerNum = Player.GamePlayers.Two;
            players[(int)GamePlayers.Two].Position = targ;

        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            
            GameState.spriteBatch.Begin();
            //draws the background
            GameState.spriteBatch.Draw(background, new Rectangle(0, 0, background.Width, background.Height), Color.White);
            if (hasLaunched == true && count == 0)
            {
                GameState.spriteBatch.Draw(ammo, new Rectangle((int)pos.X, (int)pos.Y, ammo.Width, ammo.Height), Color.White);

            }
            if (hasLaunched == true && count == 1)
            {
                GameState.spriteBatch.Draw(ammo, new Rectangle((int)pos1.X, (int)pos1.Y, ammo1.Width, ammo1.Height), Color.White);

            }
            if (hasLaunched == true && count == 2)
            {
                GameState.spriteBatch.Draw(ammo, new Rectangle((int)pos2.X, (int)pos2.Y, ammo2.Width, ammo2.Height), Color.White);

            }
            if (gameOver == true)
            {
                GameState.spriteBatch.DrawString(fntJingJing, "Game Over: ", new Vector2(300, 50), Color.White);
                endofGame = true;

            }

            GameState.spriteBatch.Draw(cannon, new Rectangle(50, 400, cannon.Width, cannon.Height), Color.Wheat);//Transparent);//SandyBrown);
            GameState.spriteBatch.Draw(target, new Rectangle(600, 400, target.Width, target.Height), Color.RosyBrown);
            GameState.spriteBatch.DrawString(fntJingJing, "Velocity: " + velocity, new Vector2(25, 350), Color.White);
            GameState.spriteBatch.DrawString(fntJingJing, "Angle: " + angle, new Vector2(25, 325), Color.White);
           
            
            GameState.spriteBatch.End();
            animSprite2.Draw();
          
        }

        public override void Update(GameTime gameTime)
        {
            //Creates a timer and sets it to be current time - endtime, make shift clock reset for getNewPosition
            TimeSpan timer = currentTime - endTime;
            animSprite2.Update();
            if (GameState.kbHandler.isKeyPressed(Keys.Escape))
            {
                GameState.game.Exit();
            }
            if (GameState.kbHandler.isKeyPressed(Keys.M))
            {
                GameState.screenManager.Pop();
                GameState.screenManager.Push(new MainMenu());
            }
            if (GameState.kbHandler.isKeyPressed(Keys.P))
            {
                GameState.screenManager.Push(new PauseScreen());
            }


            if (GameState.kbHandler.isKeyHeld(Keys.F)&& hasLaunched == false)
            {

                velocity++;

            }
            if (GameState.kbHandler.isKeyHeld(Keys.D) && hasLaunched == false)
            {

                velocity--;

            }
            if (GameState.kbHandler.isKeyPressed(Keys.T))
            {
               

                if (count == 0)
                {
                    ts[0] = gameTime.ElapsedGameTime;
                    pos = getNewPosition(ts[0]);
                    hasLaunched = true;
                    hasExploded = false;

                }
                if (count == 1)
                {
                    hasLaunched = false;

                    ts[1] = gameTime.ElapsedGameTime - gameTime.ElapsedGameTime;
                    pos1 = initial;
                    pos1 = getNewPosition(ts[1]);
                    hasLaunched = true;
                    hasExploded = false;

                }
                if (count == 2)
                {
                    hasLaunched = false;
                    ts[2] = gameTime.ElapsedGameTime - gameTime.ElapsedGameTime;
                    pos2 = initial;
                    pos2 = getNewPosition(ts[2]);
                    hasLaunched = true;
                    hasExploded = false;
                }




                GameState.sdBank.PlayCue("cannon_fire");


            }

            if (hasLaunched == true)
            {

                currentTime = currentTime.Add(gameTime.ElapsedGameTime);
                if (count == 0)
                {
                    pos = getNewPosition(currentTime);
                    players[(int)GamePlayers.One].Position = pos;

                }
                if (count == 1)
                {
                    pos1 = getNewPosition(timer);
                    players[(int)GamePlayers.One].Position = pos1;

                }
                if (count == 2)
                {
                    
                    pos2 = getNewPosition(timer);
                    players[(int)GamePlayers.One].Position = pos2;
                }



                if (players[(int)GamePlayers.One].Intersects(players[(int)GamePlayers.Two]))
                {
                    GameState.sdBank.PlayCue("explosion");

                    hasExploded = true;
                    
                    animSprite2.Play();
                  
                    
                    if (cntGm == 2)
                    {
                        velocity = 0.0f;
                    }
                    if (cntGm == 20)
                    {
                        gameOver = true;
                    }
                    cntGm++;
                }
                else
                {
                    if (players[(int)GamePlayers.One].Position.Y >= 500 && !players[(int)GamePlayers.One].Intersects(players[(int)GamePlayers.Two]))
                    {
                        GameState.sdBank.PlayCue("explosion");
                        hasLaunched = false;
                        hasExploded = true;
                        pos = initial;
                        count++;
                        endTime = currentTime;

                        if (count == 3)
                        {
                            gameOver = true;


                        }

                    }
                    

                }


            }



           

            if (GameState.kbHandler.isKeyPressed(Keys.Up) && angle < 125 && hasLaunched != true)
            {
                angle++;

            }
            if (GameState.kbHandler.isKeyPressed(Keys.Down) && angle > 15 && hasLaunched != true)
            {

                angle--;

            }
            foreach (Player p in players)
                p.Update(gameTime);

           
            if (endofGame == true)
            {
                Thread.Sleep(5000);
                GameState.eog = true;
            }



        }

        public Vector2 getNewPosition(TimeSpan startTime)
        {
            //uses the initial position adds the velocity, then multiplies by the cosine: the cosine multiples the angle by the totalseconds
            //Mathhelper converts the angle from degrees to radians
            v.X = (float)(initial.X + velocity * (float)Math.Cos(MathHelper.ToRadians(angle)) * startTime.TotalSeconds);

            //uses the initial position adds the velocity, then multiplies by the sine: the sine multiples the angle by the totalseconds which then subtracts
            // half of the gravity times time squared
            //Mathhelper converts the angle from degrees to radians.. 
            v.Y = (float)((initial.Y + velocity * (float)Math.Sin(MathHelper.ToRadians(angle)) * startTime.TotalSeconds) -
                0.5 * (GameState.Gravity * startTime.TotalSeconds * startTime.TotalSeconds));
            //Uses the opposite of the Y position and sets as a negative number and adds 800 to it, makes it so that it the arc is correct and that
            //it starts off where it should.
            v.Y = -v.Y + 800;
            return v;

        }


    }
}
