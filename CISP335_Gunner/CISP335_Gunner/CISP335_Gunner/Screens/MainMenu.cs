﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;


namespace CISP335_Gunner.Screens
{
    public class MainMenu : GameScreen
    {

        SpriteFont fntJingJing;


        Texture2D sprite;


        public MainMenu()
        {
            fntJingJing = GameState.Content.Load<SpriteFont>(@"Font\JingJing");
            sprite = GameState.Content.Load<Texture2D>(@"Sprites\mainmenu");
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            GameState.spriteBatch.Begin();

            GameState.spriteBatch.Draw(sprite, new Rectangle(0, 0, sprite.Width, sprite.Height), Color.White);

            GameState.spriteBatch.DrawString(fntJingJing, "Main Menu \n Press ESC to Exit \n Press Space to Continue to game" +
            "\n Press F to increase the velocity \n Press D to descrease the velocity \n Press T to fire the cannon ball \n Press P to pause \n Press M to go back to main menu", new Vector2(250, 100), Color.White);

            GameState.spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            if (GameState.kbHandler.isKeyPressed(Keys.Escape))
            {
                GameState.game.Exit();
            }

            if (GameState.kbHandler.isKeyPressed(Keys.Space))
            {//Need work with the game screen
                GameState.screenManager.Pop();
                GameState.screenManager.Push(new InGame());
            }

        }



    }
}
