﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace CISP335_Gunner.Screens
{
    public class SplashScreen : GameScreen
    {
        SpriteFont fntJingJing;
        Texture2D sprite;


        public SplashScreen()
        {
            fntJingJing = GameState.Content.Load<SpriteFont>(@"Font\JingJing");
            sprite = GameState.Content.Load<Texture2D>(@"Sprites\splashscreen");
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            GameState.spriteBatch.Begin();

            GameState.spriteBatch.Draw(sprite, new Rectangle(0, 0, sprite.Width, sprite.Height), Color.White);
            GameState.spriteBatch.DrawString(fntJingJing, "Welcome to my Game! \n Press Space to Continue", new Vector2(150, 150), Color.Black);


            GameState.spriteBatch.End();
        }

        public override void Update(Microsoft.Xna.Framework.GameTime gameTime)
        {

            if (GameState.kbHandler.isKeyPressed(Keys.Space))
            {
                GameState.screenManager.Pop();
                GameState.screenManager.Push(new MainMenu());
            }

        }

    }
}
