﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CISP335_Gunner.Screens
{
   public class PauseScreen : GameScreen
    {
        SpriteFont fntJingJing;


        Texture2D sprite;

        public PauseScreen()
        {
            fntJingJing = GameState.Content.Load<SpriteFont>(@"Font\JingJing");
            sprite = GameState.Content.Load<Texture2D>(@"Sprites\pause");
        }

        public override void Draw(Microsoft.Xna.Framework.GameTime gameTime)
        {
            GameState.spriteBatch.Begin();

            GameState.spriteBatch.Draw(sprite, new Rectangle(0, 0, sprite.Width, sprite.Height), Color.White);

            GameState.spriteBatch.DrawString(fntJingJing, "To Resume the game press P", new Vector2(250, 185), Color.White);

            GameState.spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            

            if (GameState.kbHandler.isKeyPressed(Keys.P))
            {//Need work with the game screen
                GameState.screenManager.Pop();
                //GameState.screenManager.Push(new InGame());
            }

        }



    }
}
