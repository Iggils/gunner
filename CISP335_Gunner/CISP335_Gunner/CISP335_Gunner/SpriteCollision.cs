﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace CISP335_Gunner
{
    public class Player
    {
        private Texture2D m_texture;
        private Vector2 m_position;
        public GamePlayers m_playerNum;
        private Color[] m_color;


        public enum GamePlayers
        {
            One = 0,
            Two
        }

        public Texture2D Texture
        {
            get 
            { 
                return m_texture; 
            }
            set 
            {
                m_texture = value;
            }
        }

        public Vector2 Position
        {
            get { return m_position; }
            set { m_position = value; }
        }

        public Boolean Intersects(Player p)
        {
            bool overlaps = false;

            Color[] cA;
            Color[] cB;

            Rectangle rA;
            Rectangle rB;

            cA = new Color[Texture.Width * Texture.Height];
            cB = new Color[p.Texture.Width * p.Texture.Height];

            Texture.GetData(cA);
            p.Texture.GetData(cB);
            


            rA = new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height);
            rB = new Rectangle((int)p.Position.X, (int)p.Position.Y, p.Texture.Width, p.Texture.Height);

            overlaps = arePixelsOverlapped(rA, cA, rB, cB);

            return overlaps;

        }

        private Boolean arePixelsOverlapped(Rectangle rectA, Color[] colorA,
            Rectangle rectB, Color[] colorB)
        {
            bool overlapped = false;

            int top = Math.Max(rectA.Top, rectB.Top);
            int bottom = Math.Min(rectA.Bottom, rectB.Bottom);
            int left = Math.Max(rectA.Left, rectB.Left);
            int right = Math.Min(rectA.Right, rectB.Right);

            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    Color cA = colorA[(x - rectA.Left) + (y - rectA.Top) * rectA.Width];
                    Color cB = colorB[(x - rectB.Left) + (y - rectB.Top) * rectB.Width];


                    if (cA.A != 0 && cB.A != 0)
                    {
                        overlapped = true;
                    }
                }
            }

            return overlapped;
        }

        public void Update(GameTime gameTime)
        {


            switch (m_playerNum)
            {

                case Player.GamePlayers.One:
                    

                    break;
                case Player.GamePlayers.Two:
                    

                    break;


            }
            

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(m_texture, m_position, Color.White);


        }
    }
}
