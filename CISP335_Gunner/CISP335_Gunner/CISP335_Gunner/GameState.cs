﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;

namespace CISP335_Gunner
{
    public class GameState
    {
        public static Game game;

        public static SpriteBatch spriteBatch;
        public static ContentManager Content;
        public static ScreenManager screenManager;
        public static KeyboardHandler kbHandler;
        public static float Gravity = 9.8f;
        public static AudioEngine auEngine;
        public static SoundBank sdBank;
        public static WaveBank wvBank;
        public static Boolean eog;
    }
}
