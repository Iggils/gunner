using System;

namespace CISP335_Gunner
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Gunner game = new Gunner())
            {
                game.Run();
            }
        }
    }
#endif
}

